extends Label

func _process(_delta):
	if Global.lives >= 0:
		self.text = "Health : " + str(Global.lives)
	if Global.lives < 0:
		self.text = "Health : 0"
