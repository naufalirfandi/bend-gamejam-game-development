extends LinkButton

export(String) var scene_to_load

func _on_LinkButton_pressed():
	Global.lives = 100
	Global.live = true
	Global.have_snow = false
	Global.have_fire = false
	Global.snow_active = false
	Global.fire_active = false
	Global.snow_cd = false
	Global.fire_cd = false
	get_tree().change_scene(str("res://Scene/" + scene_to_load + ".tscn"))
