extends TextureButton

onready var time_label = $Counter/Value

export var cooldown = 1.0

func _ready():
	time_label.hide()
	$Sweep.value = 0
	$Timer.wait_time = cooldown
	
func _process(_delta):
	if Global.have_snow == true:
		disabled = false
		$Sweep.texture_progress = texture_normal
	if Global.have_snow == false:
		disabled = true
		$Sweep.texture_progress = texture_disabled
	time_label.text = "%3.1f" % $Timer.time_left
	$Sweep.value = int(($Timer.time_left / cooldown) * 100)
	if Global.snow_active and !Global.snow_cd:
		set_process(true)
		$Timer.start()
		time_label.show()

func _on_Timer_timeout():
	$Sweep.value = 0
	time_label.hide()
