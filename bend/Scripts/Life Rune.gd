extends Area2D

var x
var y

func _on_Area2D_area_entered(area):
	if area.is_in_group("hitbox_player"):
		x = Global.lives
		y = x + 50
		if y >= 100:
			Global.lives = 100
		if y < 100:
			Global.lives = y
		get_parent().queue_free()
