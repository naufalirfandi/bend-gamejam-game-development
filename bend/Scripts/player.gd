extends KinematicBody2D

export (int) var speed = 350
export (int) var jump_speed = -600
export (int) var GRAVITY = 1200

const UP = Vector2(0,-1)

var velocity = Vector2()
var is_attacking = false
var is_knockback_kiri = false
var is_knockback_kanan = false
var is_knockback_head = false
var die = false
var animation = "idle"
var timer_cek = false
var tmp_speed = speed
var tmp_jmp_speed = jump_speed
var timer_fire
var timer_snow

func get_input():
	animation = "idle"
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed('ui_up') and !die and !is_knockback_head and !is_knockback_kanan and !is_knockback_kiri:
		velocity.y = jump_speed
	if !is_on_floor() and velocity.y < 0 and !die:
		animation = "jump"
	if !is_on_floor() and velocity.y > 0 and !die:
		animation = "drop"
	if Input.is_action_pressed('ui_right') and !die and !is_knockback_head and !is_knockback_kanan and !is_knockback_kiri:
		velocity.x += speed
		if is_on_floor() and !Global.fire_active:
			animation = "walk"
		$AnimatedSprite.flip_h = false
	if Input.is_action_pressed('ui_left') and !die and !is_knockback_head and !is_knockback_kanan and !is_knockback_kiri:
		velocity.x -= speed
		if is_on_floor() and !Global.fire_active:
			animation = "walk"
		$AnimatedSprite.flip_h = true
	if Input.is_action_just_pressed("ui_attack") or is_attacking:
		if is_on_floor() and !die and !Global.fire_active:
			if $AnimatedSprite.flip_h == false and $AnimatedSprite.frame == 1:
				$AreaPlayer/CollisionShape2D.disabled = false
			if $AnimatedSprite.flip_h == true and $AnimatedSprite.frame == 1:
				$AreaPlayer2/CollisionShape2D.disabled = false
			velocity.x = 0
			is_attacking = true
			animation = "attack"
	if Global.lives <= 0 and is_on_floor():
		animation = "die"
		velocity.x = 0
	if Global.lives <= 0 and !is_on_floor():
		animation = "die_air"
		velocity.x = 0
	if is_knockback_kanan and !die:
		if $AnimatedSprite.flip_h == true:
			animation = "knockback_kanan"
		else:
			animation = "knockback_kiri"
		velocity.x += 300
	if is_knockback_kiri and !die:
		if $AnimatedSprite.flip_h == true:
			animation = "knockback_kiri"
		else:
			animation = "knockback_kanan"
		velocity.x -= 300
	if is_knockback_head and !die:
		animation = "knockback_air"
		velocity.y = -400
	if !die:
		if $AnimatedSprite.animation != animation:
			$AnimatedSprite.play(animation)
	if Input.is_action_just_released("ui_snow_power") and !die and !Global.fire_active:
		if !Global.snow_cd and Global.have_snow:
			Global.snow_active = true
			$Area2D2/CollisionShape2D.disabled = false
			if !timer_cek:
				$Timer.wait_time = 5
				$Timer.start()
				timer_cek = true
	if Input.is_action_just_released("ui_fire_power") and !die and !Global.snow_active:
		if is_on_floor():
			if !Global.fire_cd and Global.have_fire:
				Global.fire_active = true
				$Area2D2/CollisionShape2D.disabled = false
				speed = 0
				jump_speed = 0
				if !timer_cek:
					$Timer.wait_time = 3
					$Timer.start()
					timer_cek = true
		 
func _physics_process(delta):
	velocity.y += delta * GRAVITY
	velocity.x = 0
	get_input()
	velocity = move_and_slide(velocity, UP)

func _on_AnimatedSprite_animation_finished():
	if animation == "attack":
		is_attacking = false
		$AreaPlayer/CollisionShape2D.disabled = true
		$AreaPlayer2/CollisionShape2D.disabled = true
	if animation == "knockback_kanan" or animation == "knockback_kiri":
		is_knockback_kanan = false
		is_knockback_kiri = false
	if animation == "knockback_air":
		is_knockback_head = false
	if animation == "die_air":
		Global.live = false
		$Area2D/CollisionShape2D.disabled = true
	if animation == "die":
		Global.live = false
		$Area2D/CollisionShape2D.queue_free()
		$AreaPlayer.queue_free()
		$AreaPlayer2.queue_free()
		$AnimatedSprite.queue_free()
		Global.lives = 100
		Global.live = true
		Global.have_snow = false
		Global.have_fire = false
		Global.snow_active = false
		Global.fire_active = false
		Global.snow_cd = false
		Global.fire_cd = false
		$Area2D2/CollisionShape2D.disabled = true
		get_tree().change_scene(str("res://Scene/Lose Screen.tscn"))
		die = true
		
func _on_Area2D_area_entered(area):
	if area.is_in_group("damage_box_kiri"):
		is_knockback_kiri = true
	if area.is_in_group("damage_box_kanan"):
		is_knockback_kanan = true
	if area.is_in_group("damage_head_box"):
		is_knockback_head = true

func _on_Timer_timeout():
	if timer_fire == null and timer_snow == null:
		timer_fire = Timer.new()
		timer_snow = Timer.new()
		add_child(timer_fire)
		add_child(timer_snow)
	if timer_cek:
		if Global.snow_active:
			Global.snow_active = false
			Global.snow_cd = true
			timer_snow.wait_time = 10
			timer_snow.start()
		if Global.fire_active:
			Global.fire_active = false
			speed = tmp_speed
			jump_speed = tmp_jmp_speed
			Global.fire_cd = true
			timer_fire.wait_time = 20
			timer_fire.start()
		timer_fire.connect("timeout", self, "_timeout_fire")
		timer_snow.connect("timeout", self, "_timeout_snow")
		$Area2D2/CollisionShape2D.disabled = true
		timer_cek = false

func _timeout_fire():
	Global.fire_cd = false

func _timeout_snow():
	Global.snow_cd = false
