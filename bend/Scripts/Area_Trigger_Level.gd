extends Area2D

export (String) var sceneName = "Level 1"

func _on_Area_Trigger_Level_area_entered(area):
	if area.is_in_group("hitbox_player"):
		get_tree().change_scene(str("res://Scene/" + sceneName + ".tscn"))
