extends CanvasLayer

func _ready():
	set_visible(false)

func _input(event):
	var current_scene = get_tree().get_current_scene().get_name()
	if Input.is_action_just_pressed("ui_cancel"):
		if !current_scene == "Main Menu" and !current_scene == "Win Screen" and !current_scene == "Lose Screen":
			set_visible(!get_tree().paused)
			get_tree().paused = !get_tree().paused

func _on_LinkButton_pressed():
	get_tree().paused = false
	set_visible(false)
	
func set_visible(is_visible):
	for node in get_children():
		node.visible = is_visible


func _on_LinkButton2_pressed():
	get_tree().change_scene(str("res://Scene/Main Menu.tscn"))
	get_tree().paused = false
	set_visible(false)
