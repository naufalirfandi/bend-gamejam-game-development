extends KinematicBody2D

export (int) var speed = 100
export (int) var GRAVITY = 1200
export (int) var damage = 10

const UP = Vector2(0,-1)

var velocity = Vector2()
var die = false
var is_attacked = false
var direction = 1
var snow_speed = 0.30 * speed
var tmp_speed = speed
var animation = "walk"

func get_input():
	animation = "walk"
	velocity.x = speed * direction
	if is_on_wall():
		direction *= -1
		velocity.x = speed * direction
		$RayCast2D.position.x *= -1
	if $RayCast2D.is_colliding() == false:
		direction *= -1
		velocity.x = speed * direction
		$RayCast2D.position.x *= -1
	if direction == 1 and !die:
		$AnimatedSprite.flip_h = false
	if direction == -1 and !die:
		$AnimatedSprite.flip_h = true
	if !is_attacked and !die:
		if Global.snow_active:
			animation = "walk(ice)"
		if !Global.snow_active:
			animation = "walk"
	if is_attacked and !die:
		velocity.x = 0
		$Area2D2/CollisionShape2D.disabled = true
		$Area2D3/CollisionShape2D.disabled = true
		$Area2D4/CollisionShape2D.disabled = true
		if Global.snow_active:
			animation = "die(ice)"
		if Global.fire_active:
			animation = "die(fire)"
		if !Global.fire_active and !Global.snow_active:
			animation = "die"
	if !Global.live:
		animation = "idle"
		velocity.x = 0
	if !die:
		if $AnimatedSprite.animation != animation:
			$AnimatedSprite.play(animation)

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "die" or $AnimatedSprite.animation == "die(fire)" or $AnimatedSprite.animation == "die(ice)":
		$Area2D/CollisionShape2D.queue_free()
		$CollisionShape2D.queue_free()
		$AnimatedSprite.queue_free()
		$Area2D2/CollisionShape2D.queue_free()
		$Area2D3/CollisionShape2D.queue_free()
		$Area2D4/CollisionShape2D.queue_free()
		die = true

func _on_Area2D_area_entered(area):
	if area.is_in_group("sword"):
		is_attacked = true
	if area.is_in_group("power_area"):
		if Global.snow_active:
			speed = snow_speed
		if Global.fire_active:
			is_attacked = true
			
func _on_Area2D_area_exited(area):
	if area.is_in_group("power_area"):
		speed = tmp_speed

func _on_Area2D2_area_entered(area):
	if area.is_in_group("hitbox_player"):
		Global.lives -= damage

func _on_Area2D3_area_entered(area):
	if area.is_in_group("hitbox_player"):
		Global.lives -= damage
		
func _on_Area2D4_area_entered(area):
	if area.is_in_group("hitbox_player"):
		Global.lives -= damage
