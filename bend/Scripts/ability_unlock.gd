extends Area2D

export (String) var type

func _on_ability_unlock_area_entered(area):
	if area.is_in_group("hitbox_player"):
		if type == "snow":
			Global.have_snow = true
		if type == "fire":
			Global.have_fire = true
		get_parent().queue_free()
